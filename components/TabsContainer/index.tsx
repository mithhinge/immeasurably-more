import { TabsContainerRecord } from "lib/graphql";
import styles from './tabsContainer.module.css';
import cn from 'clsx';
import { useState } from "react";
import Block from "components/Block";

export default function TabsContainer({ tabMaxWidth, tabs }: TabsContainerRecord) {
  const [activeTabIndex, setActiveTabIndex] = useState(0);

  return (
    <div className="w-full border-b border-b-gray-300">
      <ul className="w-full flex justify-center">
        {tabs.map((tab, index) => (
          <li key={tab.title} className={cn(styles.tab)} style={{maxWidth: tabMaxWidth ?? undefined}}>
            <div className={cn("font-mulish py-5 cursor-pointer font-bold",
              index === activeTabIndex && styles.active,
              index !== activeTabIndex && 'text-gray-500',
              )} onClick={() => setActiveTabIndex(index)}>
              {(tab.title)}
            </div>
          </li>
        ))}
      </ul>
      <div>
        {tabs[activeTabIndex].content &&
          tabs[activeTabIndex].content.map((block: any) => <Block key={block.id} record={block}/>)}
      </div>
    </div>
  );
}