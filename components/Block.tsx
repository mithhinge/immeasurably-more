import {
    ButtonRecord, CardsGridContainerRecord, DownloadsListRecord, FaqQuestionRecord, HeroRecord, HintTextRecord,
    ImageRecord, LeaderVideosContainerRecord, NavbarRecord,
    SectionRecord,
    StructuredTextRecord,
    TabsContainerRecord,
    TwoColumnRecord,
    TwoColumnSectionRecord,
    VideosContainerRecord,
    VideoThumbnailRecord, WordCarouselRecord
} from "lib/graphql";
import Button from "components/Button";
import Section from "components/Section";
import TwoColumn from "components/TwoColumn";
import {Image, StructuredText} from "react-datocms";
import React from "react";
import VideoThumbnail from "components/VideoThumbnail";
import WordsCarousel from "components/WordsCarousel";
import FaqQuestion from "components/FaqQuestion";
import Hero from "components/Hero";
import TabsContainer from "./TabsContainer";
import CardsGrid from "./CardsGrid";
import TwoColumnSection from "./TwoColumnSection";
import DownloadsList from "./DownloadsList";
import VideosContainer from "./VideosContainer";
import LeaderVideos from "./LeaderVideos";

export default function Block({record}: { record: any }) {
    switch (record.__typename) {
        case 'SectionRecord':
            const section = record as SectionRecord;
            return <Section {...section} />
        case 'TwoColumnSectionRecord':
            const twoColumnSection = record as TwoColumnSectionRecord;
            return <TwoColumnSection {...twoColumnSection} />
        case 'ButtonRecord':
            const button = record as ButtonRecord;
            return <Button title={button.title!} style={button.style!} link={button.link!} icon={button.icon!} iconPosition={button.iconPosition!}/>
        case 'ImageRecord':
            const {image} = record as ImageRecord;
            return image?.responsiveImage
                ? <Image className="my-4" data={image.responsiveImage!} />
                : <img className="my-4" alt={image?.alt ?? ''} src={image?.url}/>
        case 'TwoColumnRecord':
            const cols = record as TwoColumnRecord;
            return <TwoColumn {...cols} />
        case 'StructuredTextRecord':
            const textRecord = record as StructuredTextRecord;
            return <StructuredText data={textRecord.content as any} renderBlock={Block} />
        case 'VideoThumbnailRecord':
            const videoThumbnail = record as VideoThumbnailRecord;
            return <VideoThumbnail {...videoThumbnail} />
        case 'VideosContainerRecord':
            const videosContainer = record as VideosContainerRecord;
            return <VideosContainer {...videosContainer} />
        case 'WordCarouselRecord':
            const wordsCarousel = record as WordCarouselRecord;
            return <WordsCarousel {...wordsCarousel}/>
        case 'HintTextRecord':
            const hintTextRecord = record as HintTextRecord;
            return <span className="font-mulish font-medium text-sm">{hintTextRecord.text}</span>
        case 'FaqQuestionRecord':
            const faqQuestion = record as FaqQuestionRecord;
            return <FaqQuestion {...faqQuestion} />
        case 'NavbarRecord':
            const navbar = record as NavbarRecord;
            return <div></div>
        case 'HeroRecord':
            const hero = record as HeroRecord;
            return <Hero {...hero} />
        case 'TabsContainerRecord':
            const tabsContainer = record as TabsContainerRecord;
            return <TabsContainer {...tabsContainer} />
        case 'CardsGridContainerRecord':
            const cardsGrid = record as CardsGridContainerRecord;
            return <CardsGrid {...cardsGrid} />
        case 'DownloadsListRecord':
            const downloadsList = record as DownloadsListRecord;
            return <DownloadsList {...downloadsList} />
        case 'LeaderVideosContainerRecord':
            const leaderVideos = record as LeaderVideosContainerRecord;
            return <LeaderVideos {...leaderVideos} />
        default:
            return <span>{record.__typename}</span>
    }
}
