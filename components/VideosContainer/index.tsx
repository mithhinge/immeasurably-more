import cn from 'clsx';
import { VideosContainerRecord } from "lib/graphql";
import VideoThumbnail from "../VideoThumbnail";
import styles from './VideosContainer.module.css'

export default function VideosContainer({ width, videos }: VideosContainerRecord) {
  return (
    <div className={cn("sm:grid sm:grid-flow-row sm:grid-cols-2 sm:auto-rows-min sm:gap-x-6 sm:gap-y-12 gap-y-6 mt-12", styles.videosContainer)}>
      {videos.map((video) => (
        <div key={video.id} className={cn({
          'sm:col-span-2': video.size && video.size === 'large'
        })}>
          <VideoThumbnail {...video} />
        </div>
      ))}
    </div>
  )
}
