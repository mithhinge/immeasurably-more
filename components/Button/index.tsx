import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './button.module.css';
import cn from 'clsx';

export default function Button({
                                   link,
                                   title,
                                   style,
                                   icon,
                                   iconPosition,
                                   onClick
                               }: { link?: string, title: string, style: string, icon?: string, iconPosition?: string, onClick?: () => void }) {
    const iconLeading = !iconPosition || iconPosition === 'leading';
    const iconEl = icon && (<FontAwesomeIcon className={cn({
        'mr-2': iconLeading,
        'ml-2': !iconLeading,
    })} icon={icon as IconProp} />);
    return (
        <a className={`${styles.button} ${style}`} href={link ?? '#'} onClick={onClick ?? undefined}>
            {iconLeading && iconEl}
            {title}
            {!iconLeading && iconEl}
        </a>
    );
}
