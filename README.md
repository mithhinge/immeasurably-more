# Immeasurably More

## Requirements
- 
- Node v16+ 

## Getting Started

To run the development server:

```bash
npm run dev
# or
yarn dev
```

To build the app;
```
npm run build
```


## Linter

```
npm run lint
```

## Configuration

You should set `NEXT_DATO_CMS_API_TOKEN` env variable to an API token with view permissions.

> Note that whenever you update the schema or graphql queries you should run `npm run codegen` to update graphql client.


## DatoCMS configuration

- All the resources can be controled from DatoCMS.
- You can access the test account here https://dashboard.datocms.com/project/85369 with these credentials

email: `zaklance96@gmail.com`

password: `test123@!`

I have let the token be in .env file for testing purposes. Please remove it before commit.
