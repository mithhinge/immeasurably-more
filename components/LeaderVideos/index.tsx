import { LeaderVideosContainerRecord } from "lib/graphql";
import cn from 'clsx';
import styles from './LeaderVideos.module.css';
import { Image } from "react-datocms";
import { useState } from "react";
import VideoModal from "components/VideoModal";

export default function LeaderVideos({ videos }: LeaderVideosContainerRecord) {
  const [showVideoModal, setShowVideoModal] = useState(false);
  const [videoLink, setVideoLink] = useState('');
  return (
    <div className="flex flex-col sm:flex-row flex-wrap justify-center">
      {videos.map((video) => (
        <div
          key={video.id}
          className={cn("flex items-center justify-start rounded-lg overflow-hidden mx-3 mt-4 bg-white hover:shadow-lg transition-shadow cursor-pointer", styles.leaderVideo)}
          onClick={() => {
            setVideoLink(video.link!);
            setShowVideoModal(true);
          }}>
          <div className="h-full w-fit relative items-center justify-center flex flex-col mr-2">
                <img className="h-full w-auto" src={video.thumbnail!.responsiveImage!.src}/>
                <img className="!absolute" alt="play" src="/icons/play-button.svg" width={24} height={24} />
          </div>
          <p className="!text-base font-bold mr-2">
            {video.title}
          </p>
          <p className="!text-base text-gray-400">
            {video.duration}
          </p>
        </div>
      ))}
      {showVideoModal && videoLink && <VideoModal src={videoLink} setShowModal={setShowVideoModal} />}
    </div>
  )
}