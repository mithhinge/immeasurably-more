import { DownloadsListRecord } from "lib/graphql";
import cn from 'clsx';
import Button from "../Button";
import styles from './DownloadsList.module.css';

export default function DownloadsList({ width, backgroundColor, content }: DownloadsListRecord) {
  return (
    <ul className={cn(`rounded-2xl ${backgroundColor} mt-8`, styles.downloadsList)} style={{width}}>
      {content.map((listItem, index) => (
        <li key={listItem.id} className={cn("flex justify-between px-5 py-6 items-center border-gray-200", {
          'border-b': index !== content.length - 1,
        })}>
          <p><strong>{listItem.title}</strong></p>
          <Button title={listItem.buttonText!} style={listItem.buttonStyle!} icon={listItem.icon!} iconPosition={listItem.iconPosition!}/>
        </li>
      ))}
    </ul>
  )
}