import {TwoColumnSectionRecord} from "lib/graphql";
import {StructuredText} from 'react-datocms'
import React from "react";
import Block from "components/Block";
import cn from 'clsx'
import styles from './twoColumnSection.module.css';


export default function TwoColumnSection({
                                    sectionIdentifier,
                                    maxWidth,
                                    leftColumn,
                                    rightColumn,
                                    backgroundImage,
                                    backgroundColor,
                                    verticalPadding,
                                }: TwoColumnSectionRecord) {
    return (<div className={cn(`two-column-section relative w-full min-h-[300px] flex flex-col items-center justify-center ${backgroundColor} section${sectionIdentifier}`, styles.twoColumnSection)} style={{
        backgroundImage: `url('${backgroundImage?.responsiveImage?.src}')`,
    }}>
        <div style={{maxWidth: maxWidth || undefined}}
             className={cn("w-full px-4 flex flex-col items-center", {
                 'py-8': verticalPadding && verticalPadding === 'small',
                 'py-16': !verticalPadding || verticalPadding === 'regular',
                 'py-16 sm:py-24': verticalPadding && verticalPadding === 'large',
                 'max-w-[805px]': !maxWidth || maxWidth === 0,
             })}>
            <div className="relative w-full flex flex-col items-center justify-center my-4 sm:flex-row">
                <div className={cn("flex-1 child-p-justify sm:mx-3 mx-8 sm:order-2 right-column")}>
                    <StructuredText data={rightColumn as any} renderBlock={Block}/>
                </div>
                <div className={cn("flex-1 child-p-justify sm:mx-3 sm:order-1 left-column")}>
                    <StructuredText data={leftColumn as any} renderBlock={Block}/>
                </div>
            </div>
        </div>
    </div>)
}
