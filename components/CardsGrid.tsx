import { CardsGridContainerRecord, CardsGridItemRecord } from "lib/graphql";
import cn from 'clsx';
import { Image } from "react-datocms";

export default function CardsGrid({ numRows, numColumns, gap, gridItemWidth, grid }: CardsGridContainerRecord) {
  return (
    <div className="lg:grid lg:grid-flow-col mt-12" style={{
        gridTemplateRows: `repeat(${numRows}, minmax(0, 1fr))`,
        gridTemplateColumns: `repeat(${numColumns}, minmax(0, 1fr))`,
        gap
      }}>
      {grid.map((gridItem) => {
        return (
          <div key={gridItem.id}
            className="hover:shadow-2xl rounded-2xl transition-shadow mt-4 lg:mt-0"
            style={{
              width: gridItemWidth,
              gridRowStart: gridItem.rowIndex + 1,
              gridColumnStart: gridItem.columnIndex + 1,
            }}
            >
            {gridItem.image.map((img, index) => (
              <div key={img.id}
                className={cn({
                    'hidden': index === 0,
                    'sm:flex': index === 0,
                    'sm:hidden': index === 1,
                })}>
                  <Image data={img.responsiveImage!} />
              </div>
            ))}
          </div>
        )
      })}
    </div>
  );
}
