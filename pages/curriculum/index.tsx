import {QueryListenerOptions, useQuerySubscription} from "react-datocms";
import {CurriculumPageDocument, CurriculumPageModelHeaderField} from "lib/graphql";
import {sdk} from "lib/datocms";
import {InferGetStaticPropsType} from "next";
import Block from "components/Block";
import Layout from "components/Layout";
import Navbar from "components/Navbar";
import styles from "./curriculum.module.css";
import cn from 'clsx';

export const getStaticProps = async () => {
    const subscription: QueryListenerOptions<any, any> = {
        query: CurriculumPageDocument.loc?.source.body!,
        initialData: await sdk.CurriculumPage(),
        token: process.env.NEXT_DATO_CMS_API_TOKEN!,
        enabled: true,
    }

    return {
        props: {
            subscription,
        },
    };
}

export default function CurriculumPage({subscription}: InferGetStaticPropsType<typeof getStaticProps>) {
    const {
        data,
    } = useQuerySubscription(subscription);

    const {curriculumPage, common} = data
    return (
        <Layout {...common} >
            <Navbar/>
            <div className={cn("body-nav-padding lg:body-nav-padding-lg", styles.curriculumBody)}>
                {curriculumPage.header.map((b: CurriculumPageModelHeaderField) => <Block key={b.id} record={b} />)}
                {curriculumPage?.content &&
                    curriculumPage.content.map((block: any) => <Block key={block.id} record={block}/>)
                }
            </div>
        </Layout>
    )
}
