import '@fortawesome/fontawesome-svg-core/styles.css';

import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { faArrowDown } from '@fortawesome/free-solid-svg-icons';

// FontAwesome configuration (dynamic 'require' is used to mitigate hydration error bug in fontawesome - see issue: https://github.com/FortAwesome/Font-Awesome/issues/19348)
const { config, library } = require('@fortawesome/fontawesome-svg-core');
const { faPlay } = require('@fortawesome/free-solid-svg-icons');
config.autoAddCss = false
// Add icons as necessary
library.add(faPlay, faArrowDown);

export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
