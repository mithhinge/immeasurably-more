import { VideoThumbnailRecord} from 'lib/graphql';
import {motion} from 'framer-motion';
import {Image} from "react-datocms";
import { useState } from 'react';
import VideoModal from './VideoModal';



export default function VideoThumbnail({title, image, link}: VideoThumbnailRecord) {
    const [showModal, setShowModal] = useState(false);

    return <motion.div
    initial={{translateY: 100}}
    whileInView={{translateY: 0}}
    viewport={{ once: true, margin: '-100px' }}
    className="video-thumbnail w-full relative m-0">
        <a target="_blank" className="cursor-pointer my-4 sm:my-8"
        onClick={() => setShowModal(true)}>
            <div className="w-full relative items-center justify-center flex flex-col">
                <Image className="w-full h-auto" data={image!.responsiveImage!} layout="responsive" />
                <img className="!absolute" alt="play" src="/icons/play-button.svg" width={80} height={80} />
            </div>
            {title && (<p className="text-dark1 font-bold !mt-2">{title}</p>)}
        </a>
        {showModal && <VideoModal src={link!} setShowModal={setShowModal}/>}
    </motion.div>
}
