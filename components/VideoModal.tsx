import { isLocalURL } from "next/dist/shared/lib/router/router";
import { isAbsoluteUrl } from "next/dist/shared/lib/utils";
import { Dispatch, SetStateAction } from "react";

export default function VideoModal({src, setShowModal}: {
  src: string,
  setShowModal: Dispatch<SetStateAction<boolean>>
}) {
  return (
    <div className="fixed w-full h-full top-0 left-0 z-40 bg-black bg-opacity-40">
      <div className="w-screen h-screen relative flex justify-center items-center" onClick={() => setShowModal(false)}>
        <video controls muted loop className="absolute max-w-sm sm:max-w-xl md:max-w-3xl" onClick={(e) => e.stopPropagation()}>
          <source src={src && (isLocalURL(src) || isAbsoluteUrl(src)) ? src : "https://elevationchurch.org/app/uploads/2022/10/103022_ChristineCaine_ECdotOrg.mp4"} type="video/mp4"></source>
        </video>
      </div>
    </div>
  );
}
